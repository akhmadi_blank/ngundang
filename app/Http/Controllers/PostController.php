<?php

namespace App\Http\Controllers;

use App\Models\post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        //dd(request('search'));
        //------------------cara pertama---------------------
        // $posts = post::latest();
        // if (request('search')) {
        //     $posts->where('judul', 'like', '%' . request('search') . '%')
        //         ->orWhere('detail', 'like', '%' . request('search') . '%');
        // }
        // note: pada dasarnya proses diatas harusnya di lakukan oleh model,dan karena proses pencarian ini selalu di gunakan maka kita menggunakan 
        //menjadi local scope karena fungsi itu akan kita panggil saat kita membutuhkan.*baca documentasi nya
        // return view('blog', [
        //     "title" => "blog",
        //proses eiger bisa dipindah kemodel
        //"posts" => post::with(['category', 'user'])->get()
        //     'posts' => $posts->get()
        // ]);

        //------------------cara kedua---------------------
        // return view('blog', [
        //     "title" => "blog",
        //     'posts' => Post::latest()->Filter()->get()
        // ]);
        //------------------cara ketiga---------------------

        $title = '';
        if (request('category')) {
            $category = Category::firstWhere('slug', request('category'));
            $title = "in $category->nama";
        }
        if (request('user')) {
            $user = User::firstWhere('username', request('user'));
            $title = "by $user->name";
        }
        return view('blog', [
            "title" => "All Blog " . $title,
            "active" => "blogs",
            'posts' => Post::latest()->Filter(request(['search', 'category', 'user']))->paginate(7)->withQueryString() //with query guna tetap ada paginate walau di pencarian
        ]);
    }
    public function show(Post $post)
    {
        return view('detail', [
            "title" => "detail",
            "active" => "detail",
            "blog" => $post->load(['category', 'user'])
        ]);
    }

    public function categories(Category $category)
    {
        return view('category', [
            'title' => 'categories',
            "active" => "categories",
            'categories' => $category->all()
        ]);
    }
}
