<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.index', [
            'title' => 'Register',
            'active' => 'register'
        ]);
    }
    public function store(Request $request)
    {
        //return request()->all();
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'username' => 'required|unique:users|min:3',
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:3|max:255'
        ]);

        //melakukan hashing bisa mengunakan becryp atau menggunakan Hashing
        //$validateData['password'] = bcrypt($validateData['password']);
        $validateData['password'] = Hash::make($validateData['password']);

        //input kedalam database
        User::create($validateData);

        //mengirim flasdata
        //cara satu
        //$request->session()->flash('success', 'your registration has successfully');

        //cara kedua bisa menggunakan with yang disemat di return
        return redirect('/login')->with('success', 'your registration has successfully');
    }
}
