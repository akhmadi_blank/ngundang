<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    use HasFactory;
    use Sluggable;
    protected $guarded = [];
    // protected $fillable = ['name', 'username', 'email', 'password'];
    protected $with = ['category', 'user']; //contoh penggunakan easger loading jika di gunakan pada modal
    public function category()
    {
        return $this->belongsTo(Category::class, 'Category_id');
    }
    public function user()
    {
        return $this->belongsTo(user::class, 'user_id');
    }

    //-----------lanjutan cara kedua yang menggunakan local scope-------------
    // public function scopeFilter($query)
    // {
    //     if (request('search')) {
    //         return $query->where('judul', 'like', '%' . request('search') . '%')
    //             ->orWhere('detail', 'like', '%' . request('search') . '%');
    //     }
    // }

    //--------------------------------cara ketiga------------------------------
    //note* kita tidak seharusnya memanggil request di model karena memang seharusnya ada di post karena nya kita menggunakan dynamic scope
    public function scopeFilter($query, array $filter) //bisa menggunakan parameter tetapi saya gunakan array untuk memudahkan saya guna tidak dalam melakukan pencarian beberapa parameter 
    {
        //    cara 3.1

        // if (isset($filter['search']) ? $filter['search'] : false) {
        //     return $query->where('judul', 'like', '%' . $filter['search'] . '%')
        //         ->orWhere('detail', 'like', '%' . $filter['search'] . '%');
        // }

        //    cara 3.2
        //bisa juga menggunkan when bawaan dari laravel guna mengganti fungsi if *hanya mengganti notasi aja
        //null coalescing operator->bawaan php 7(cek documentasi php)->menggantikan ternary dan isset

        $query->when($filter['search'] ?? false, function ($query, $search) {
            return $query->where(function ($query) use ($search) {
                $query->where('judul', 'like', '%' . $search . '%')
                    ->orWhere('detail', 'like', '%' . $search . '%');
            });
        }); //sejujurnya saya lebih suka cara yang 3.1 jika ingin melakukan pencarian yang komplek bisa liat tutuorial di https://www.youtube.com/watch?v=YzGH6odpwpc&list=PLFIM0718LjIWiihbBIq-SWPU6b6x21Q_2&index=18
        $query->when($filter['category'] ?? false, function ($query, $category) {
            return $query->whereHas('category', function ($query) use ($category) { //wherehas guna melakukan pencarian yang melibatkan table relasi dengan yang lain
                $query->where('slug', $category);
            });
        });
        $query->when(
            $filter['user'] ?? false,
            fn ($query, $user) => //menggunakan arraw function untuk penulisan lebih sederhana
            $query->whereHas(
                'user',
                fn ($query) =>
                $query->where('username', $user)
            )
        );
    }

    //costomizing the key so that when we using route model binding the key not defult using id
    //note:this method only used when we using resources controllers
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'judul'
            ]
        ];
    }
}
