<?php

namespace App\Models;



class post
{


    private static $blog_post = [
        [
            "judul" => "postingan pertama",
            "slug" => "postingan-pertama",
            "penulis" => "akhmadi",
            "category" => "opini",
            "excerp" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus rem numquam tempora dicta? Odit corporis beatae cupiditate facere omnis dolores distinctio nobis, totam sint magni voluptate repudiandae, sed modi? Commodi modi quo quae consequatur. Aperiam, voluptates, alias animi magni eveniet incidunt aliquid aspernatur cumque quae tempore recusandae quo consequatur. Facilis!",
            "lengkap" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione hic provident velit vero aperiam exercitationem earum, tempore omnis expedita cum veritatis rerum esse minima nemo quibusdam aliquam rem laudantium quae odit voluptatum est totam excepturi incidunt perferendis. Dolore soluta dignissimos laudantium dolorum architecto. Reprehenderit, temporibus qui? Dignissimos laboriosam labore iste et, nesciunt sequi voluptas sint facilis earum nisi totam animi tempore odit eum minus repellat sapiente quaerat hic consequuntur! Reprehenderit, rem facilis, at culpa dolore eaque delectus neque recusandae nulla perferendis beatae ipsum est voluptatibus exercitationem ea! Voluptates ratione eos, quae consectetur accusamus eligendi illum doloribus, atque impedit eum molestias!"
        ], [
            "judul" => "postingan kedua",
            "slug" => "postingan-kedua",
            "penulis" => "sukron",
            "category" => "berita",
            "excerp" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus rem numquam tempora dicta? Odit corporis beatae cupiditate facere omnis dolores distinctio nobis, totam sint magni voluptate repudiandae, sed modi? Commodi modi quo quae consequatur. Aperiam, voluptates, alias animi magni eveniet incidunt aliquid aspernatur cumque quae tempore recusandae quo consequatur. Facilis!",
            "lengkap" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione hic provident velit vero aperiam exercitationem earum, tempore omnis expedita cum veritatis rerum esse minima nemo quibusdam aliquam rem laudantium quae odit voluptatum est totam excepturi incidunt perferendis. Dolore soluta dignissimos laudantium dolorum architecto. Reprehenderit, temporibus qui? Dignissimos laboriosam labore iste et, nesciunt sequi voluptas sint facilis earum nisi totam animi tempore odit eum minus repellat sapiente quaerat hic consequuntur! Reprehenderit, rem facilis, at culpa dolore eaque delectus neque recusandae nulla perferendis beatae ipsum est voluptatibus exercitationem ea! Voluptates ratione eos, quae consectetur accusamus eligendi illum doloribus, atque impedit eum molestias!"
        ]
    ];

    public static function all()
    {
        return collect(self::$blog_post);
    }

    public static function find($slug)
    {
        return static::all()->firstWhere("slug", $slug);
    }
}
