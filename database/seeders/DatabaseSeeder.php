<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use \App\Models\post;
use App\Models\User;
use Illuminate\Support\Str;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Akhmadi",
            "username" => "Akhmadi",
            "email" => "akhmadi.blank@gmail.com",
            'email_verified_at' => now(),
            "password" => bcrypt('password'),
            'remember_token' => Str::random(10),
        ]);
        User::factory(10)->create();
        post::factory(20)->create();
        Category::factory()->create([
            "nama" => "programing",
            "slug" => "programing"
        ]);
        Category::factory()->create([
            "nama" => "personal",
            "slug" => "personal"
        ]);
        Category::factory()->create([
            "nama" => "blog",
            "slug" => "blog"
        ]);
    }
}
