@extends('layouts.main')
@section('container')
   <div class="container">
       <div class="row">
           <div class="col-md-12">
              
                <h1 class="text-center">{{ $title }}</h1>
                <div class="row justify-content-center">
                  <div class="col-md-6">
                    <form action="/blog" method="get">
                      @if (request('category'))
                        <input type="hidden" name="category" value="{{ request('category') }}">
                        @elseif (request('user'))
                        <input type="hidden" name="user" value="{{ request('user') }}">
                      @endif
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Search..." name="search" value="{{ request('search') }}">
                        <button class="btn btn-danger" type="submit" id="btn-search">Search</button>
                      </div>
                    </form>
                  </div>
                </div>
                @if ($posts->count())
                <div class="card mb-3">
                  @if ($posts[0]->image)
                  <div style="max-height: 400px;overflow:hidden">
                    <img src="{{ asset('storage/'.$posts[0]->image) }}" class="card-img-top mb-2" alt="{{ $posts[0]->category->nama }}">  
                  </div>
                  @else
                  <img src="https://source.unsplash.com/1200x500/?{{ $posts[0]->category->nama }}" class="card-img-top" alt="...">
                  @endif
                   
                    <div class="card-body text-center">
                      <a href="/blog/{{ $posts[0]->slug }}" class="text-decoration-none text-dark"><h3 class="card-title">{{ $posts[0]->judul }}</h3></a>
                      <p>
                        <small class="text-muted">
                        By Penulis: <a href="/blog?user={{$posts[0]->user->username}}"class="text-decoration-none">{{ $posts[0]->user->name }}</a> in <a href="/blog?category={{ $posts[0]->category->slug}}" class="text-decoration-none">{{ $posts[0]->category->nama}}</a> {{ $posts[0]->created_at->diffForHumans() }}
                        </small>
                      </p>
                      <p class="card-text">{!! $posts[0]->excerp !!}</p>
                      <a href="/blog/{{ $posts[0]->slug }}" class="text-decoration-none btn btn-primary">Read More</a>
                      
                    </div>
                  </div>
               
        
                <div class="container">
                    <div class="row">
                        @foreach ($posts->skip(1) as $post )
                        <div class="col-md-4 mb-3 d-flex align-items-stretch">
                            <div class="card">
                                <div class="category"><a href="/blog?category={{ $post->category->slug}}"class="text-decoration-none text-white position-absolute px-2 py-2" style="background-color:rgba(0, 0, 0, 0.535);">{{ $post->category->nama}}</a></div>
                                @if ($post->image)
                                <div style="max-height: 350px;overflow:hidden">
                                  <img src="{{ asset('storage/'.$post->image) }}" class="card-img-top mb-2" alt="{{ $post->category->nama }}">  
                                </div>
                                @else
                                <img src="https://source.unsplash.com/1200x600/?{{$post->category->nama}}" class="card-img-top mb-2" alt="{{ $post->category->nama }}">
                                @endif
                                {{-- <img src="https://source.unsplash.com/500x500/?{{$post->category->nama}}" class="card-img-top" alt="{{ $post->category->nama }}"> --}}
                                <div class="card-body">
                                    <a href="/blog/{{ $post->slug }}" class="text-decoration-none text-dark"><h3 class="card-title">{{ $post->judul }}</h3></a>
                                    <p>
                                        <small class="text-muted">
                                        By Penulis: <a href="/blog?user={{$post->user->username}}"class="text-decoration-none">{{ $post->user->name }}</a> {{ $posts[0]->created_at->diffForHumans() }}
                                        </small>
                                      </p>
                                      <p class="card-text">{{ $post->excerp }}</p>
                                      <a href="/blog/{{ $post->slug }}" class="text-decoration-none btn btn-primary">Read More</a>
                                </div>
                              </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                @else
                <p class="text-center fs-4">No Post Found.</p>
                @endif

                <div class="d-flex justify-content-center">
                  @if ($title==="All Blog")
                    {{ $posts->links() }}
                  @endif
                </div>
            
           </div>
        </div>
   </div>
@endsection