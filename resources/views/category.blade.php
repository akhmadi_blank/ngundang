@extends('layouts.main')
@section('container')
   <div class="container">
       <div class="row">
           <div class="col-md-12">
               <h1 class="text-center my-4">{{ $title }}</h1>
               <div class="container">
                   <div class="row">
                       @foreach ($categories as $category )
                       <div class="col-md-4 mb-3">
                           <a href="/blog?category={{ $category->slug}}">
                               <div class="card bg-dark text-white">
                                   <img src="https://source.unsplash.com/500x500/?{{$category->nama}}" class="card-img-top" alt="{{ $category->nama }}">
                                   <div class="card-img-overlay d-flex align-items-center p-0 text-center fs-3 ">
                                       <h5 class="card-title flex-fill" style="background-color:rgba(0, 0, 0, 0.7);">{{ $category->nama}}</></h5>
                                   </div>
                                   
                                 </div>
                           </a>
                       </div>
                       @endforeach
                   </div>
               </div>

           </div>
        </div>
   </div>
@endsection