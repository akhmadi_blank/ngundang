@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Edit Your Posts</h1>
</div>
<div class="row">
  <div class="col-lg-10">
    <form action="/dashboard/posts/{{ $post->slug }}" method="post" enctype="multipart/form-data">
      @method('put')
      @csrf
      <div class="mb-3">
        <label for="judul" class="form-label">Title</label>
        <input type="text" class="form-control @error('judul')is-invalid @enderror" required autofocus value="{{ old('judul',$post->judul) }}" id="judul" name="judul">
        @error('judul')
        <div id="validationServer03Feedback" class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="slug" class="form-label">Slug</label>
        <input type="text" class="form-control @error('slug')is-invalid @enderror" required autofocus value="{{ old('slug',$post->slug) }}" id="slug" name="slug" readonly>
        @error('slug')
        <div id="validationServer03Feedback" class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="Category" class="form-label">Category</label>
        <select class="form-select" name="category_id">
          @foreach ($categories as $category )
          @if (old('category_id',$post->Category_id)==$category->id)
          <option selected value="{{ $category->id }}">{{ $category->nama }}</option>
          @else
          <option value="{{ $category->id }}">{{ $category->nama }}</option>
          @endif
          @endforeach
        </select>
      </div>
      <div class="mb-3">
        <label for="image" class="form-label">Picture</label>
        @if ($post->image)
          <img src="{{ asset('storage/'.$post->image) }}" class="img-preview d-block mb-3 col-sm-3"alt="">
        @else
        <img class="img-preview img-fluid mb-3 col-sm-3">
        @endif
        <input class="form-control @error('image')is-invalid @enderror" type="file" id="image" name="image"   onchange="previewImage()">
        @error('image')
        <div id="validationServer03Feedback" class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        @error('detail')
        <p class="text-danger">
          {{ $message }}
        </p>
        @enderror
        <label for="detail" class="form-label">Detail</label>
        <input id="detail" type="hidden" name="detail" value="{{ old('detail',$post->detail) }}">
        <trix-editor input="detail"></trix-editor>
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
</div>

{{-- make sluggable using java script and using  eloquent sluggable on https://github.com/cviebrock/eloquent-sluggable--}}
<script>
  const judul = document.querySelector('#judul');
  const slug = document.querySelector('#slug');

  judul.addEventListener('change', function() {
    console.log(judul.value);
    fetch('/dashboard/posts/checkslug?title=' + judul.value)
      .then(response => response.json())
      .then(data => slug.value = data.slug)
  });
  //untuk mematikan fungsi attach file di trix editor
  document.addEventListener('trix-file-accept', function(e) {
    e.preventDefault;
  });


  ///below is function to preview image when upload file image  
  function previewImage(){
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('.img-preview');

    imgPreview.style.display='block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);
    oFReader.onload=function(oFREvent){
      imgPreview.src = oFREvent.target.result;
    }
  }
</script>
@endsection