@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">My Posts</h1>  
</div>
<div class="row">
  <div class="col-lg-10">
    @if (session('success'))
    <div class="alert alert-success" role="alert">
      {{ session('success') }}
    </div>
    @endif

    {{-- @if (session('delete'))
    <div class="alert alert-success" role="alert">
      {{ session('delete') }}
    </div>
    @endif --}}
    <a href="/dashboard/posts/create" class="btn btn-primary mb-2">Create New Post</a>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Title</th>
          <th scope="col">Category</th>
          <th scope="col">action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($posts as $post)
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $post->judul }}</td>
          <td>{{ $post->category->nama }}</td>
          <td>
            <a href="/dashboard/posts/{{ $post->slug }}" class="badge bg-primary text-dark"><span data-feather="eye"></span></a>
            <a href="/dashboard/posts/{{ $post->slug }}/edit" class="badge bg-warning text-dark"><span data-feather="edit"></span></a>
            <form action="/dashboard/posts/{{ $post->slug }}" method="post" class="d-inline">
              @method('delete')
              @csrf
              <button class="badge bg-danger text-dark border-0" onclick="return alert('are you sure?')"><span data-feather="x"></span></button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection