@extends('dashboard.layouts.main')
@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <div class="row">
    <div class="col-lg-10">
        <article class="mt-2">
            {{-- bisa memakai notasi array seperti [""] tetapi  lebih baik menggunakan notasi object --}}
               
               <h2> {{$post->judul  }} </h2>
                <a href="/dashboard/posts" class="btn btn-primary my-3"><span data-feather="arrow-left"></span> Bact to All My Post</a>               
                <a href="/dashboard/posts/{{ $post->slug }}/edit" class="btn btn-warning my-3"><span data-feather="edit"></span> Edit</a>               
                <a href="" class="btn btn-danger my-3"><span data-feather="trash-2"></span> Delete</a>               
               {{-- <div class="category"><a href="/blog?category={{ $post->category->slug}}"class="text-decoration-none text-white position-absolute px-2 py-2" style="background-color:rgba(0, 0, 0, 0.535);">{{ $post->category->nama}}</a></div> --}}
               @if ($post->image)
               <div style="max-height: 350px;overflow:hidden">
                 <img src="{{ asset('storage/'.$post->image) }}" class="card-img-top mb-2" alt="{{ $post->category->nama }}">  
               </div>
               @else
               <img src="https://source.unsplash.com/1200x600/?{{$post->category->nama}}" class="card-img-top mb-2" alt="{{ $post->category->nama }}">
               @endif
               <p>{!! $post->detail !!}</p>

              
        </article>
    </div>
</div>
@endsection