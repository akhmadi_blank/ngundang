@extends('layouts.main')
@section('container')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <article class="mt-2 mb-4">
                {{-- bisa memakai notasi array seperti [""] tetapi  lebih baik menggunakan notasi object --}}
                    <h1 class="py-2">Halaman Post</h1>
                   <h2> {{$blog->judul  }} </h2>
                   <h5>By Penulis: <a href="/blog?user={{ $blog->user->username }}">{{ $blog->user->name }}</a> in <a href="/blog?category={{ $blog->category->slug}}">{{ $blog->category->nama}}</a></h5>
                   <div class="category"><a href="/blog?category={{ $blog->category->slug}}"class="text-decoration-none text-white position-absolute px-2 py-2" style="background-color:rgba(0, 0, 0, 0.535);">{{ $blog->category->nama}}</a></div>
                   @if ($blog->image)
                   <div style="max-height: 350px;overflow:hidden">
                     <img src="{{ asset('storage/'.$blog->image) }}" class="card-img-top mb-2" alt="{{ $blog->category->nama }}">  
                   </div>
                   @else
                   <img src="https://source.unsplash.com/1200x600/?{{$blog->category->nama}}" class="card-img-top mb-2" alt="{{ $blog->category->nama }}">
                   @endif
                   <p>{!!$blog["detail"]!!}</p>

                   <a class="btn btn-primary" href="/blog">Back to blog</a>
            </article>
        </div>
    </div>
</div>
@endsection

