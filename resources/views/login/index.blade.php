@extends('layouts.main')

@section('container')
<div class="row justify-content-center">
    <div class="col-md-5">
      <main class="form-signin">
          @if (session('success'))
          <div class="mt-3 alert alert-success alert-dismissible fade show" role="alert">
            <strong>congratulation!</strong> {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif
          @if (session('loginError'))
          <div class="mt-3 alert alert-danger alert-dismissible fade show" role="alert">
            <strong>warning</strong> {{ session('loginError') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif
            <h1 class="h3 mt-3 mb-3 fw-normal text-center">Please login</h1>
            <form method="post" action="/login">
              @csrf
              <div class="form-floating">
                <input type="email" name="email" class="form-control @error('email')is-invalid @enderror" id="email" placeholder="name@example.com" autofocus required>
                <label for="email">Email address</label>
                @error('email')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
              <div class="form-floating">
                <input type="password" class="form-control  @error('password') is-invalid @enderror" id="password" placeholder="Password" name="password" required autofocus>
                <label for="password">Password</label>
                @error('password')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
              <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
            </form>
            <small class="d-block text-center mt-3">Not Registered?<a href="/register">Register Now</a></small>
        </main>
    </div>
</div>
@endsection