<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\DashboardPostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        "title" => "home",
        "active" => "home"
    ]);
});
Route::get('/about', function () {
    return view('about', [
        "title" => "about",
        "active" => "about"
    ]);
});
Route::get('/blog', [PostController::class, 'index']);
Route::get('/blog/{post:slug}', [PostController::class, 'show']);
Route::get('/categories', [PostController::class, 'categories']);

//dibawah ini sudah tidak di pakai karena sudah ditangani oleh filter didalam controler post

// Route::get('/categories/{category:slug}', function (Category $category) {  
//     return view('blog', [
//         'title' => "Post By Category : $category->nama",
//         'posts' => $category->post
//     ]);
// });
// Route::get('/users/{user:username}', function (User $user) {
//     return view('blog', [
//         'title' => "Post By Author : $user->name",
//         'posts' => $user->Post
//     ]);
//     // return view('about');
// });

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);

Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/dashboard', function () {
    return view('dashboard.index');
})->middleware('auth');

Route::get('dashboard/posts/checkslug', [DashboardPostController::class, 'checkslug'])->middleware('auth');
Route::resource('dashboard/posts', DashboardPostController::class)->middleware('auth');

Route::resource('dashboard/categories', AdminCategoryController::class)->except('show')->middleware('is_admin');
